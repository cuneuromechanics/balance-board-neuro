#-------------------------------------------------
#
# Project created by QtCreator 2014-06-27T21:05:28
#
#-------------------------------------------------

# We're not actually using the Qt library for this
# We might re-write in the future, however, to
# make better use of the events and concurrency model
QT       -= core gui

TARGET = wiiuse
TEMPLATE = lib
# One less binary file floating around is a victory.
# Maybe it'd made sense to make this dynamically
# linked if it were a plugin and a user could choose
# from multiple types of devices besides Wii things.
CONFIG += staticlib
DEFINES += WIIUSE_STATIC

# At least in windows, this will be relative to
# the output directory configured in the .pro.user
# file.
DESTDIR = ../lib

# These source files were initial added only
# for the windows project, but the initial library
# was designed to be cross-platform.  It could
# be cross-platform again with a little work.
SOURCES += \
    classic.c \
    dynamics.c \
    events.c \
    guitar_hero_3.c \
    io.c \
    ir.c \
    motion_plus.c \
    nunchuk.c \
    util.c \
    wiiboard.c \
    wiiuse.c

win32:SOURCES += io_win.c

HEADERS += \
    classic.h \
    definitions.h \
    dynamics.h \
    events.h \
    guitar_hero_3.h \
    io.h \
    ir.h \
    motion_plus.h \
    nunchuk.h \
    os.h \
    wiiboard.h \
    wiiuse.h \
    wiiuse_internal.h \
    wiiuse_msvcstdint.h

win32:LIBS += Setupapi.lib
win32:LIBS += Ws2_32.lib
win32:LIBS += hid.lib

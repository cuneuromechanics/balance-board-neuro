#include "bluetoothinterface.h"

#include "bluetoothstructures.h"

/**
 * @brief BluetoothInterface::BluetoothInterface
 * @param parent
 *
 * Prepare 'watcher' items to respond when we finish threads.
 * Initialize WinSock stuff.
 */
BluetoothInterface::BluetoothInterface(QObject *parent) :
  QObject(parent)
{
  radioHandle = nullptr;
  notifyHandle = nullptr;
  connect(&inquiryWatcher,SIGNAL(finished()),this,SLOT(inquiryComplete()));
  connect(&authWatcher,SIGNAL(finished()),this,SLOT(authComplete()));

  activeInquiry = false;

  WSADATA  WSAData = {0};
  WSAStartup(MAKEWORD(2, 2), &WSAData);

  bts = new BluetoothStructures;
}

BluetoothInterface::~BluetoothInterface()
{
  // Wait for threads to finish, if necessary
  inquiryFuture.waitForFinished();
  authFuture.waitForFinished();

  // We no longer need notification of bluetooth events
  closeEventCapture();

  // Close up WinSock resources
  WSACleanup();

  // de-allocate memory
  delete bts;
}

/**
 * @brief BluetoothInterface::threadComplete callback to let us know
 * that the inquiry thread is done.
 */
void BluetoothInterface::inquiryComplete()
{
  activeInquiry = false;
  emit debugString("Inquiry complete");
  emit finishedInquiry();
}

/**
 * @brief BluetoothInterface::authComplete callback to let us know that
 * the authentication process finished.
 */
void BluetoothInterface::authComplete()
{
  emit debugString(QString("Authentication result: %1").arg(authFuture.result()));
}

/**
 * @brief BluetoothInterface::pairing
 * @param toggle
 *
 *
 */
void BluetoothInterface::pairing(bool toggle)
{
  if (toggle) {
    beginInquiry();
  }
}

bool BluetoothInterface::findBluetoothRadio()
{
  BLUETOOTH_FIND_RADIO_PARAMS btfrp = {sizeof(BLUETOOTH_FIND_RADIO_PARAMS)};
  // Get the radio handle
  HBLUETOOTH_RADIO_FIND radioSearch = BluetoothFindFirstRadio(&btfrp,&radioHandle);
  // Todo: Need some error checking here
  // There can be more than one bluetooth radio in a given machine.
  // Optionally, we can allow the user to select which radio we'll use.
  // if (radioHandle) emit foundRadio();

  // Free resources used to enumerate bt radios
  BluetoothFindRadioClose(radioSearch);

  if (radioHandle) {

    bts->radioInfo.dwSize = sizeof(BLUETOOTH_RADIO_INFO);
    BluetoothGetRadioInfo(radioHandle,&(bts->radioInfo));
//    emit debugString(
//          QString("Local Addr: %1:%2:%3:%4:%5:%6")
//          .arg(radioInfo.address.rgBytes[0])
//          .arg(radioInfo.address.rgBytes[1])
//          .arg(radioInfo.address.rgBytes[2])
//          .arg(radioInfo.address.rgBytes[3])
//          .arg(radioInfo.address.rgBytes[4])
//          .arg(radioInfo.address.rgBytes[5])
//        );


  } else {
    // We've got trouble
  }

  // We don't need to put the radio into discoverable mode.

//  if (radioHandle) {
//    wasDiscoverable = BluetoothIsDiscoverable(radioHandle);
//    if (!wasDiscoverable) {
//      if (BluetoothEnableDiscovery(radioHandle,TRUE)) {
//        emit debugString("Enabled discovery");
//        if (BluetoothIsDiscoverable(radioHandle)) {
//          emit debugString("Now discoverable");
//        }
//      } else {
//        emit debugString("Couldn't enable discovery");
//      }
//    } else {
//      emit debugString("Already discoverable");
//    }
//  }

  return (radioHandle!=nullptr);
}

/**
 * @brief BluetoothInterface::beginEventCapture takes a windows handle
 * (it's necessary to call show() first or else the handle will be
 *  null) and registers it to receive bluetooth events.
 * @param wid
 * @return true on success
 */
bool BluetoothInterface::beginEventCapture(WId wid)
{
  HWND hwnd = (HWND) wid;
  // We can't receive messages without a window handle
  // and a device
  if (!hwnd || !radioHandle) return false;

  DEV_BROADCAST_HANDLE dbh = {0};
  dbh.dbch_size = sizeof(DEV_BROADCAST_HANDLE); // Set size
  dbh.dbch_devicetype = DBT_DEVTYP_HANDLE;      // Set type
  dbh.dbch_handle = radioHandle;                // Give the bluetooth handle
  dbh.dbch_eventguid = GUID_BLUETOOTH_RADIO_IN_RANGE; // Ask when radios come into range
  // Apparently, this registers the window for all bt
  // events, not just "radio_in_range" events.

  notifyHandle = RegisterDeviceNotification(hwnd,
                             &dbh,
                             DEVICE_NOTIFY_WINDOW_HANDLE);
  return (notifyHandle!=nullptr);
}

/**
 * @brief BluetoothInterface::closeEventCapture tells Windows that
 * this application is no longer interested in receiving DEVICE_CHANGE
 * events.
 */
void BluetoothInterface::closeEventCapture()
{
  if (notifyHandle) {
    UnregisterDeviceNotification(notifyHandle);
    notifyHandle=nullptr;
  }
}


/**
 *
 */
static BOOL CALLBACK btAuthCallback(_In_opt_ LPVOID pvParam, _In_ PBLUETOOTH_AUTHENTICATION_CALLBACK_PARAMS pAuthCallbackParams)
{
  //printf("Authentication method: %d\n",pAuthCallbackParams->authenticationMethod);
  // TODO: Check to make sure that we're using legacy authentication
  BLUETOOTH_AUTHENTICATE_RESPONSE response = {0};
  response.bthAddressRemote = pAuthCallbackParams->deviceInfo.Address;
  response.authMethod = pAuthCallbackParams->authenticationMethod;

  BluetoothStructures* bts = (BluetoothStructures*) pvParam;

  for (int ii=0;ii<6;++ii) {
    // When a wiimote becomes discoverable using the (1) and (2) buttons,
    // the proper response is to send the devices its own address in
    // reverse as a pin.

    // response.pinInfo.pin[ii] = response.bthAddressRemote.rgBytes[ii];

    // Balance boards don't have a "button (1) or (2)".  When a device
    // becomes discoverable through the 'sync' button, the proper response
    // is to send the local bluetooth radio address in reverse as a pin.
    response.pinInfo.pin[ii] = bts->radioInfo.address.rgBytes[ii];
  }
  response.pinInfo.pinLength=6;
  BluetoothSendAuthenticationResponseEx(nullptr,&response);

  // Return value is ignored
  return true;

  // TODO: make note of the address we just authenticated.

}

static DWORD attemptAuthentication(BLUETOOTH_DEVICE_INFO* btdi)
{
  return BluetoothAuthenticateDeviceEx(nullptr,nullptr,btdi,nullptr,MITMProtectionNotRequired);
}


/**
 * @brief BluetoothInterface::deviceChange receives notification of
 * DEVICE_CHANGE events.
 * @param msg
 * @param result
 * @return
 */
bool BluetoothInterface::deviceChange(MSG* msg,long* result)
{
  // This is the event we want
  if (DBT_CUSTOMEVENT == msg->wParam) {
    DEV_BROADCAST_HANDLE* dbh = (DEV_BROADCAST_HANDLE*) msg->lParam;

    if (IsEqualGUID(dbh->dbch_eventguid,GUID_BLUETOOTH_RADIO_IN_RANGE)) {
      BTH_RADIO_IN_RANGE* rir = (BTH_RADIO_IN_RANGE*)dbh->dbch_data;
      // Check for a valid device class
      if ((rir->deviceInfo.flags&BDIF_COD) &&
          (rir->deviceInfo.classOfDevice == 0x2504))
      {
        emit debugString("Radio in range message");

        emit debugString(rir->deviceInfo.name);
        emit debugString(QString("Paired: %1 Pers: %2 Connected: %3")
                         .arg(rir->deviceInfo.flags&BDIF_PAIRED)
                         .arg(rir->deviceInfo.flags&BDIF_PERSONAL)
                         .arg(rir->deviceInfo.flags&BDIF_CONNECTED));
        if (rir->deviceInfo.flags&BDIF_ADDRESS) {
          //addMap[rir->deviceInfo.address] = true;

          // If the device isn't currently connected
          // Then we can try to authenticate and connect.
          if (!(rir->deviceInfo.flags&BDIF_CONNECTED)&&activeInquiry) {
            BLUETOOTH_DEVICE_INFO copyInfo = {0};
            copyInfo.dwSize = sizeof(BLUETOOTH_DEVICE_INFO);
            copyInfo.Address.ullLong = rir->deviceInfo.address;
//            copyInfo.ulClassofDevice = 0;
//            copyInfo.fConnected = 0;
//            copyInfo.fRemembered = 0;
//            copyInfo.fAuthenticated = 0;


            // Pin is the host address in reverse
            WCHAR pin[6];
            for (int ii=0;ii<6;++ii) {
              pin[ii] = copyInfo.Address.rgBytes[ii];
            }

            // Sometimes we get ERROR_DEVICE_NOT_CONNECTED
            // Why?

            DWORD tst = BluetoothRegisterForAuthenticationEx(&copyInfo,&(devHandle), btAuthCallback, bts);
            emit debugString("Register for authentication "+QString::number(tst));
            authFuture = QtConcurrent::run(attemptAuthentication,&copyInfo);
            authWatcher.setFuture(authFuture);

          }
        }
      }


    } else if (IsEqualGUID(dbh->dbch_eventguid,GUID_BLUETOOTH_RADIO_OUT_OF_RANGE)) {
      //BLUETOOTH_ADDRESS* bta = (BLUETOOTH_ADDRESS*) dbh->dbch_data;
//      if (addMap.count(bta->ullLong)) {
//        emit debugString("Radio out of range");
//        emit debugString(QString("Addr: 0x%1").arg(bta->ullLong,6,16));
//      }
    } else if (IsEqualGUID(dbh->dbch_eventguid,GUID_BLUETOOTH_HCI_EVENT)) {
      emit debugString("HCI Event");
    } else if (IsEqualGUID(dbh->dbch_eventguid,GUID_BLUETOOTH_L2CAP_EVENT)) {
      emit debugString("L2CAP event");
      BTH_L2CAP_EVENT_INFO* ei = (BTH_L2CAP_EVENT_INFO*)dbh->dbch_data;
      emit debugString(QString("Channel: %1 Connect: %2 Init: %3")
                       .arg(ei->psm).arg(ei->connected).arg(ei->initiated) );
    } else if (IsEqualGUID(dbh->dbch_eventguid,GUID_BLUETOOTH_HCI_VENDOR_EVENT)) {
    } else if (IsEqualGUID(dbh->dbch_eventguid,GUID_BLUETOOTH_AUTHENTICATION_REQUEST)) {
      emit debugString("BT Authentication request");
    } else {
      emit debugString("Unknown message");
      emit debugString(
            QString("%1-%2-%3-%4%5-%6%7%8%9%10%11")
            .arg(dbh->dbch_eventguid.Data1,8,16)
            .arg(dbh->dbch_eventguid.Data2,4,16)
            .arg(dbh->dbch_eventguid.Data3,4,16)
            .arg(dbh->dbch_eventguid.Data4[0],2,16)
            .arg(dbh->dbch_eventguid.Data4[1],2,16)
            .arg(dbh->dbch_eventguid.Data4[2],2,16)
            .arg(dbh->dbch_eventguid.Data4[3],2,16)
            .arg(dbh->dbch_eventguid.Data4[4],2,16)
            .arg(dbh->dbch_eventguid.Data4[5],2,16)
            .arg(dbh->dbch_eventguid.Data4[6],2,16)
            .arg(dbh->dbch_eventguid.Data4[7],2,16)
            );

    }
    *result = 0;
    return true;
  }
  return false;
}

/**
 * @brief BluetoothInterface::beginInquiry tells the
 * local bluetooth radio to wake up and look around for
 * other devices.
 */
void BluetoothInterface::beginInquiry()
{
  if (activeInquiry) return;
  activeInquiry = true;

  emit debugString("Starting inquiry");
  inquiryFuture = QtConcurrent::run(this,&BluetoothInterface::inquiry);
  inquiryWatcher.setFuture(inquiryFuture);
}

void BluetoothInterface::inquiry()
{
  BLUETOOTH_DEVICE_SEARCH_PARAMS btsp = {0};
  BLUETOOTH_DEVICE_INFO btdi = {0};

  btsp.dwSize = sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS);
  // Ask for all devices of all kinds
  btsp.fReturnAuthenticated=TRUE;
  btsp.fReturnConnected=TRUE;
  btsp.fReturnRemembered=TRUE;
  btsp.fReturnUnknown=TRUE;
  btsp.hRadio=radioHandle;
  btsp.cTimeoutMultiplier=8; // Look for about 10 seconds at a time.
  btsp.fIssueInquiry=TRUE;

  btdi.dwSize = sizeof(BLUETOOTH_DEVICE_INFO);

  // We don't actually do anything with the inquiry result
  // because
  // we capture and respond to the DEVICE_CHANGE events.
  // so that we can start the authentication process
  // more quickly.
  // We don't get the events
  // unless we're in active inquiry.
  HBLUETOOTH_DEVICE_FIND deviceSearch = BluetoothFindFirstDevice(&btsp,&btdi);;
  if (deviceSearch) {
    BluetoothFindDeviceClose(deviceSearch);
  }
}

/**
 * @brief BluetoothInterface::queryBluetooth
 * Make a search for bluetooth devices without
 * making formal inquiry.
 *
 * This finds remembered devices and tells us
 * if they're authenticated.  If they are,
 * then one-button authentication connection
 * should work from the device.
 *
 * However, we'll still re-pair with any
 * device that we see during inquiry in case
 * the device has forgotten the Host address.
 */
void BluetoothInterface::queryBluetooth()
{
  BLUETOOTH_DEVICE_SEARCH_PARAMS btsp = {0};
  BLUETOOTH_DEVICE_INFO btdi = {0};

  btsp.dwSize = sizeof(BLUETOOTH_DEVICE_SEARCH_PARAMS);
  // Ask for all devices
  btsp.fReturnAuthenticated=TRUE;
  btsp.fReturnConnected=TRUE;
  btsp.fReturnRemembered=TRUE;
  btsp.fReturnUnknown=TRUE;
  btsp.hRadio=radioHandle;
  // Don't send out inquiry
  btsp.cTimeoutMultiplier=0;
  btsp.fIssueInquiry=FALSE;

  btdi.dwSize = sizeof(BLUETOOTH_DEVICE_INFO);

  HBLUETOOTH_DEVICE_FIND deviceSearch = BluetoothFindFirstDevice(&btsp,&btdi);;
  bool findingDevices = deviceSearch!=nullptr;
  while (findingDevices) {
    //processDevice(btdi);
    QString devName = QString::fromWCharArray(btdi.szName);
    emit debugString(QString("Found bt device: %1").arg(devName));
    if (btdi.fConnected) debugString("Connected");
    if (btdi.fAuthenticated) debugString("Authenticated");
    if (btdi.fRemembered) debugString("Remembered");

//    if (btdi.ulClassofDevice==0x2504) {
//      printf("Connecting to peripheral...\n");
//      BLUETOOTH_DEVICE_INFO copyInfo = btdi;
//      handles.push_back(0);
//      DWORD tst = BluetoothRegisterForAuthenticationEx(&btdi,&(handles[handles.size()-1]), btAuthCallback, nullptr);
//      DWORD err = BluetoothAuthenticateDeviceEx(nullptr,nullptr,&copyInfo,nullptr,MITMProtectionNotRequired);
//    }

    findingDevices = BluetoothFindNextDevice(deviceSearch,&btdi);
  }
  if (deviceSearch) {
    BluetoothFindDeviceClose(deviceSearch);
  }
}



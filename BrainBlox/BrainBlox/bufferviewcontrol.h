#ifndef BUFFERVIEWCONTROL_H
#define BUFFERVIEWCONTROL_H

#include <QWidget>

#include <QToolButton>
#include <vector>
#include "BoardDataFrame.h"


#include "plotsceneview.h"
#include "qcustomplot.h"


namespace Ui {
class BufferViewControl;
}

class WiiDevice;

/**
 * @brief The BufferViewControl class provides an interface
 * to specify options for each data buffer for all of the
 * possible Views.
 *
 * In the future, we might want to make controllers for
 * each view-buffer pair.
 *
 * We'll also show a summary of the data: the range and average.
 * Should that be computed here or in the buffer?
 *
 */
class BufferViewControl : public QWidget
{
  Q_OBJECT

public:
  explicit BufferViewControl(WiiDevice* device,
                             PlotSceneView* rView,PlotSceneView* fView,QCustomPlot* tView,
                             QWidget *parent = 0);
  ~BufferViewControl();

  void attachDevice(WiiDevice* device);

  void setViews(PlotSceneView* rView,PlotSceneView* fView,QCustomPlot* tView);

  bool saveData(const QString& filename);

public slots:
  void setViewScale(int ww,int hh); ///< Choose scale factors according to the viewport size

  void setBatteryLevel(float percent);

  void recordToggle(bool shouldRecord);
  void clearBuffer();
  void saveDialog();

  void checkButton();
  void releaseButton();

  void setRecentActive(bool isActive);
  void setTimeActive(bool isActive);
  void setFullActive(bool isActive);

  void showXTPlot(bool isSet);
  void showYTPlot(bool isSet);
  void showFTPlot(bool isSet);
  void changeXWidth(int pix);
  void changeYWidth(int pix);
  void changeFWidth(int pix);

  void setHistoryLength(int length);

  // Slots to allow color selection
  // We'll want some default colors, how'll
  // we manage that?
  void currentColorDialog();
  void recentColorDialog();
  void oldColorDialog();
  void xColorDialog();
  void yColorDialog();
  void fColorDialog();
  void fullColorDialog();

  void setCurrentColor(const QColor& color);
  void setRecentColor(const QColor& color);
  void setOldColor(const QColor& color);
  void setXColor(const QColor& color);
  void setYColor(const QColor& color);
  void setFColor(const QColor& color);
  void setFullColor(const QColor& color);

  void addFrame(float x,float y,float f,qint64 tStamp);
  void updateViews();

protected:
  void setToolButtonColor(QToolButton* button,const QColor& color);
  void interpolateColors();
  QColor colorMix(double frac) const;

protected:
  Ui::BufferViewControl *ui;
  // Our initial design had a separate buffer object
  // but it ended up being little more than a vector
  // so it might as well sit directly inside this
  // object
  std::vector<BoardDataFrame> data; ///< All our data
  BoardDataFrame mostRecent;
  WiiDevice* wiiDevice; ///< The device we control and get data from

  // Colors for displaying data in the various views
  QColor currentColor;
  QColor oldColor;
  QColor recentColor;
  QColor fullColor;
  QColor xColor;
  QColor yColor;
  QColor fColor;

  // Time plot graphs
  QCPGraph* xGraph;
  QCPGraph* yGraph;
  QCPGraph* fGraph;

  // RecentView items
  QGraphicsEllipseItem* currentItem;
  std::vector<QGraphicsEllipseItem*> recentHistory;

  // FullView items
  float lastX,lastY;
  std::vector<QGraphicsLineItem*> fullHistory;

  // The buffer views:
  // This object doesn't own these widgets.  They
  // belong to the tabWidget in the MainWindow.
  PlotSceneView* recentView; ///< The window showing current data
  PlotSceneView* fullView;   ///< The window showing the entire trace
  QCustomPlot*   timeView;   ///< The window showing components over time

  // Which views are currently active?
  bool recentActive;
  bool fullActive;
  bool timeActive;

  size_t recentFrame;
  size_t fullFrame;
  size_t timeFrame;



  bool recording;

  float minX,maxX,meanX,varX;
  float minY,maxY,meanY,varY;
  float minF,maxF,meanF,varF;

};

/*
 * In order to do its job, the bufferviewcontrol needs
 * access to both the buffers and the views.
 * It makes sense for this object to hold a handle
 * to its device.  It makes sense for it to actually
 * hold the buffer.  However, it's annoying to pass in
 * all the different buffers.  However, we'll do it anyway.
 *
 * I considered just making the views be a static variable.
 * But having them passed in makes it so that, in the future,
 * we can create separate displays for different devices.
 *
 * Then we can display the devices in a split window or
 * something.
 *
 * In order to have multiple buffers feeding into a single
 * view, we need to move the display logic into the buffer
 * control instead of in the view.
 *
 * To save cycles(?) we only update the active view.
 * That means that we need to know which view(s) is(are) active.
 * This is super annoying because they're different classes.
 * We might need to sub-class them to provide a common interface.
 * Essentially, when we update, we need to ask each view if it's
 * currently active.  If a view is active, we need to figure out
 * what additionaly data points need to be poured in to get it
 * up-to-date.
 *
 */

#endif // BUFFERVIEWCONTROL_H

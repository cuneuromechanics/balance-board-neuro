#ifndef WIIUSEHANDLER_H
#define WIIUSEHANDLER_H

#include <QObject>
#include <QMap>

#include "wiidevice.h"

struct wiimote_t;
#define MAX_WIIMOTES				4

/**
 * @brief The WiiuseHandler class encapsulates the Wiiuse library
 *
 * Handles events and passes them out in Qt style.
 *
 * We also manage Bluetooth connectivity:
 *   Find the local BT radio.
 *   Listen for Events
 *   Allow pairing
 *   Maintain a map of device filenames
 *   Poll for new devices (checking against present devices)
 *
 */
class WiiuseHandler : public QObject
{
  Q_OBJECT
public:
  /// Constructor attempts to create
  explicit WiiuseHandler(QObject *parent = 0);
  virtual ~WiiuseHandler();

  void wrapNewDevice(const QString& fid,wiimote_t* dev); ///< Take a wiiuse handle and wrap it in our object

signals:
  void connected(int); ///< How many devices are found
  void newDevice(WiiDevice*);

public slots:
  /// The wiiuse library isn't event driven, so
  /// we need to call this frequently to keep the
  /// device updated.
  void poll();

  // This uses the HID API to query for devices that
  // match the Wii profile.  If there are some, it
  // opens an HID link and initiates handshaking.
  // If its a Balance Board, it starts streaming data.
  //void initDevices(bool toOpen);

  /// Rather than only add devices at the beginning,
  /// we can check for new devices periodically.
  /// If we find a new one, we can open a handle to it
  /// and add it to our list without significant pain.
  void findNewDevices();

  /// Close the connections to all the devices.
  void disconnect();

  /// Ask devices for status updates (to keep track of battery).
  /// There's probably a better way to do this.  Presumably,
  /// we could read from a particular part of memory.
  void requestStatus();


public:
  //wiimote_t** wiimotes; ///< wiiuse data type holding the device handles

  //WiiDevice* device[MAX_WIIMOTES]; ///< Our representation of the devices
  //int devCount;



  QMap<QString,WiiDevice*> deviceMap; ///< A record of which handles we have open

};

#endif // WIIUSEHANDLER_H

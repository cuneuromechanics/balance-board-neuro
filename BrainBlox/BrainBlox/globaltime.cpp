#include "globaltime.h"

QElapsedTimer GlobalTime::timer;

//static
void GlobalTime::start()
{
  timer.start();
}

//static
qint64 GlobalTime::elapsed()
{
  return timer.elapsed();
}

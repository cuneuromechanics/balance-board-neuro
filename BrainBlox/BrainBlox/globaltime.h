#ifndef GLOBALTIME_H
#define GLOBALTIME_H


#include <QElapsedTimer>

/**
 * @brief The GlobalTime class provides a simple globally
 * accesible clock that tracks how long the program has
 * been running.
 *
 * This might be better handled with global objects and
 * functions in a Namespace...
 *
 * Would there be any difference besides the fact that
 * you can technically instantiate a "GlobalTime"
 * object, but that doesn't really make sense?
 *
 */
class GlobalTime
{
protected:
  static QElapsedTimer timer;
public:
  static void start();
  static qint64 elapsed();
};

#endif // GLOBALTIME_H

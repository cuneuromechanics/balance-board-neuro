#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QColorDialog>
#include <QInputDialog>
#include <QFileDialog>
#include <QToolButton>
#include <QMessageBox>

#include <QTextEdit>

#include "fullcustomplot.h"

#include "globaltime.h"



MainWindow::MainWindow(QWidget *parent) :
  QMainWindow(parent),
  ui(new Ui::MainWindow)
{
  ui->setupUi(this);   // Initialize the GUI
  GlobalTime::start(); // Start tracking elapsed time for timestamps

  // Create a device handler for managing the Balance Boards
  handler = new WiiuseHandler(this);
  // When the connect button is clicked, tell the device handler
  connect(ui->connectPushButton,SIGNAL(clicked()),this,SLOT(connectPrepare()));

  connect(ui->recordAllPushButton,SIGNAL(clicked()),this,SLOT(recordAll()));
  connect(ui->stopAllPushButton,SIGNAL(clicked()),this,SLOT(stopAll()));
  connect(ui->clearAllPushButton,SIGNAL(clicked()),this,SLOT(clearData()));
  connect(ui->saveAllPushButton,SIGNAL(clicked()),this,SLOT(saveData()));

  // When the device handler gets a new device, create a buffer for it
  // and a controller.
  connect(handler,SIGNAL(newDevice(WiiDevice*)),this,SLOT(createBuffer(WiiDevice*)));


  // ***** We should report how many devices we see and provide
  // an interface for linking directly.
  // Eventually, we should stick the linking code straight into
  // the wiiuse library... although the threads make that annoying.


  // Create our various BufferViews
  recentView = new PlotSceneView;
  timeView = new QCustomPlot;
  fullView = new FullCustomPlot;
  initializeCustomPlot();

  recentIdx = ui->tabWidget->addTab(recentView,"Recent Trace");
  timeIdx = ui->tabWidget->addTab(timeView,"Time Plot");
  fullIdx = ui->tabWidget->addTab(fullView,"Full Trace");

  connect(ui->tabWidget,SIGNAL(currentChanged(int)),this,SLOT(tabChanged(int)));

  // ////////////////////////////////////////////////////
  // Setup timers to poll the devices to feed the Buffers
  // and also to update the BufferViews.
  // ////////////////////////////////////////////////////

  ui->timeEdit->setTime(QTime(0,2));

  // Ask the devices for a status update every 5 seconds.
  statusTimer.setInterval(5000);
  statusTimer.setSingleShot(false);
  connect(&statusTimer,SIGNAL(timeout()),handler,SLOT(requestStatus()));

  // Repaint the active view 60 times per second
  viewTimer.setInterval(1000/60);
  viewTimer.setSingleShot(true); // We'll try to avoid falling behind.
  connect(&viewTimer,SIGNAL(timeout()),this,SLOT(updateView()));

  // Poll the devices very frequently
  deviceTimer.setInterval(1);
  deviceTimer.setSingleShot(false);
  connect(&deviceTimer,SIGNAL(timeout()),handler,SLOT(poll()));

  connect(&recordTimer,SIGNAL(timeout()),this,SLOT(stopAll()));

  // Showing the window creates a handle we can use
  // to capture Windows events
  this->show();

  // This a debug string helping us
  // know what's going on with the Bluetooth.
  // Ideally, we'll extract a summary and show
  // it in the pairing dialog
  if (1) {
    QTextEdit* textEdit = new QTextEdit;
    ui->tabWidget->addTab(textEdit,"BT Debug");
    connect(&mybt,SIGNAL(debugString(QString)),
            textEdit,SLOT(append(QString)));
  }


  // Ensure the presence of a BT radio
  if (!mybt.findBluetoothRadio()) {
    QMessageBox::critical(this,"Problem","No Bluetooth radio found!");
  } else {
    // Wake up the Bluetooth radio
    mybt.beginEventCapture(this->winId());
    mybt.beginInquiry();
  }

  pDialog = new PairingDialog(this);

  dataBuffs = devBuffs = 0;

  connect(pDialog,SIGNAL(pairing(bool)),&mybt,SLOT(pairing(bool)));
  connect(&mybt,SIGNAL(finishedInquiry()),pDialog,SLOT(endPairing()));

  // We'll probably need to move this to the end.
  statusTimer.start();
  viewTimer.start();
  deviceTimer.start();


}

MainWindow::~MainWindow()
{
  delete ui;
}


/**
 * @brief MainWindow::updateView needs to know which view is currently active.
 *
 * The BufferView has handles to the the ViewControllers.  They provide data
 * and tell how to display that data.  It also knows what frame it grabbed
 * last.  So it will ask the Buffers for whatever new data may be available
 * and then paint the data appropriately.
 *
 * We'll update all visible views at once: we can have dockable tabs so that
 * multiple views are visible at a time.
 */
void MainWindow::updateView()
{
  int kk = viewControlList.size();
  for (int cc = 0;cc < kk;++cc) {
    viewControlList[cc]->updateViews();
  }
  if (fullView->isVisible()) {
    fullView->replot();
  }
  if (timeView->isVisible()) {
    timeView->xAxis->setRange(GlobalTime::elapsed()/1000.0+0.25, 8, Qt::AlignRight);
    timeView->replot();
  }
  if (recordTimer.isActive()) {
    QTime timeout(0,0);
    int rTime = recordTimer.remainingTime();
    ui->timeEdit->setTime(timeout.addMSecs(rTime));
  }
  viewTimer.start();
}

/**
 * @brief MainWindow::tabChanged
 * @param newTab
 *
 * A new tab just became visible.  We can
 * correct the aspect ratio and such.
 */
void MainWindow::tabChanged(int /*newTab*/)
{
  // Unfortunately, this doesn't work.
  // This function gets called _before_
  // the graph is updated.
//  if (newTab==fullIdx) {
//    fullView->yAxis->setScaleRatio(fullView->xAxis);
//  }
}

void MainWindow::connectPrepare()
{
  handler->findNewDevices();
}

/**
 * @brief MainWindow::launchPairing opens a dialog
 * that informs the user to press the 'sync' button.
 * It also launches an inquiry thread that sees the
 * device enter discoverable mode.
 * It registers this window for authentication
 * and initiates pairing with the Wii device's expected
 * code (the address reversed).
 * If the pairing works out, we should be notified
 * that a new L2CAP connection has been made.
 * Thereafter, the device should automatically seek out
 * the paired computer when any button (except sync)
 * is pressed.
 */
void MainWindow::launchPairing()
{
  pDialog->exec();
}

void MainWindow::loadBoardDialog()
{
  QString filename = QFileDialog::getOpenFileName(this,"File to load");
  if (!filename.isEmpty()) {
    QFile file(filename);
    file.open(QFile::ReadOnly);
    QTextStream in(&file);

    std::vector<BoardFrame> buffer;
    BoardFrame frame;

    // We need a way to figure out what kind of data
    // are in this file.  A simple header will do it.
    // The problem with a header is that it makes the
    // data harder to plot with other tools.
    // Or we could just count how many elements are
    // in a row, but that's ugly.

    while (!in.atEnd()) {
        float tx,ty,tf;
      in >> frame.tStamp >> frame.tl >> frame.tr >> frame.bl >> frame.br >> tx >> ty >> tf;
      buffer.push_back(frame);
    }
    if (!buffer.empty()) {
      cloneBuffer(buffer);
    }
  }
}

/**
 * @brief MainWindow::loadAccelDialog
 *
 * It's terrible practice for us to rely on
 * the user to know which kind of file they're
 * loading.  But it's easiest for proof of
 * concept software.
 *
 * We'll need to improve this in the future.
 */
void MainWindow::loadAccelDialog()
{
    QString filename = QFileDialog::getOpenFileName(this,"File to load");
    if (!filename.isEmpty()) {
      QFile file(filename);
      file.open(QFile::ReadOnly);
      QTextStream in(&file);

      std::vector<AccelFrame> buffer;
      AccelFrame frame;

      while (!in.atEnd()) {
        in >> frame.tStamp >> frame.ax >> frame.ay >> frame.az;
        buffer.push_back(frame);
      }
      if (!buffer.empty()) {
        cloneBuffer(buffer);
      }
    }
}

/**
 * @brief MainWindow::recordAll iterates through all devices and sets them to record
 */
void MainWindow::recordAll()
{
  if (!recordTimer.isActive()) {
    int msec = ui->timeEdit->time().msecsTo(QTime(0,0));
    recordTimer.start(-msec);
  }

  auto vcEnd = viewControlList.end();
  for (auto vcIt = viewControlList.begin();
       vcIt!=vcEnd;++vcIt)
  {
    (*vcIt)->recordToggle(true);
  }
}

/**
 * @brief MainWindow::stopAll stops all devices from recording
 */
void MainWindow::stopAll()
{
  if (recordTimer.isActive()) {
    recordTimer.stop();
  } else {
    ui->timeEdit->setTime(QTime(0,2));
  }

  auto vcEnd = viewControlList.end();
  for (auto vcIt = viewControlList.begin();
       vcIt!=vcEnd;++vcIt)
  {
    (*vcIt)->recordToggle(false);
  }
}

/**
 * @brief MainWindow::clearData clears all data on all devices
 */
void MainWindow::clearData()
{
  auto vcEnd = viewControlList.end();
  for (auto vcIt = viewControlList.begin();
       vcIt!=vcEnd;++vcIt)
  {
    (*vcIt)->clearBuffer();
  }
}

/**
 * @brief MainWindow::saveData takes writes all device data to file.
 *
 * The best behavior in this case is unclear.  Should we open a file
 * dialog for each device and write everything to separate files or
 * should we take a single file name and then write out all the data
 * from all the files into that file?  Or should we append something
 * onto the filename for each file, possibly sticking them all into
 * a common folder?
 */
void MainWindow::saveData()
{

}


/**
 * @brief MainWindow::createBuffer
 * @param device
 *
 * Create a new buffer and push it onto our
 * list of buffers.  Create a new BufferScrollControl
 * and hook it up appropriately.
 */
void MainWindow::createBuffer(WiiDevice* device)
{
  BufferScrollControl* viewControl =
      new BufferScrollControl(device,recentView,fullView,timeView);
  viewControlList.push_back(viewControl);

  connect(viewControl,SIGNAL(makeClone(std::vector<BoardFrame>)),
          this,SLOT(cloneBuffer(std::vector<BoardFrame>)));


  ui->controlTabWidget->addTab(viewControl,QString::number(++devBuffs));
}

void MainWindow::cloneBuffer(const std::vector<BoardFrame>& staticData)
{
  BufferScrollControl* viewControl =
      new BufferScrollControl(staticData,recentView,fullView,timeView);
  viewControlList.push_back(viewControl);

  // Tell the buffer which views are active:
  connect(viewControl,SIGNAL(closing(BufferScrollControl*)),this,SLOT(closeBuffer(BufferScrollControl*)));

  ui->controlTabWidget->addTab(viewControl,QString::number(--dataBuffs));
}

void MainWindow::cloneBuffer(const std::vector<AccelFrame>& staticData)
{
    BufferScrollControl* viewControl =
        new BufferScrollControl(staticData,recentView,fullView,timeView);
    viewControlList.push_back(viewControl);

    // Tell the buffer which views are active:
    connect(viewControl,SIGNAL(closing(BufferScrollControl*)),this,SLOT(closeBuffer(BufferScrollControl*)));

    ui->controlTabWidget->addTab(viewControl,QString::number(--devBuffs));
}

void MainWindow::closeBuffer(BufferScrollControl* buffer)
{
  if (buffer) {
    viewControlList.removeOne(buffer);
    buffer->deleteLater();
  }
}

void MainWindow::about()
{
  QMessageBox::about(this,"About Balance Board Brain Blox",
    "Developed by Joseph Cooper at the University of Colorado Boulder\n"
    "under the direction of Dr. Alaa Ahmed in the Neuromechanics lab.\n\n"
    "This is GPL licensed, free software designed for educational use.\n"
    "This software makes use of QCustomPlot by Emanuel Eichhammer.\n"
    "Communication with the devices is supported by a modified version\n"
    "of the WiiUse library, initially developed by Michael Laforest");
}

void MainWindow::aboutQt()
{
  QMessageBox::aboutQt(this);
}

void MainWindow::tips()
{
//  QMessageBox::information(this,"Tips",
//    "Full ");
}

void MainWindow::initializeCustomPlot()
{
  // Top and bottom axes show time
  timeView->xAxis->setTickLabelType(QCPAxis::ltDateTime);
  timeView->xAxis->setDateTimeSpec(Qt::UTC);
  timeView->xAxis->setDateTimeFormat("hh:mm:ss");
  timeView->xAxis->setAutoTickStep(false);
  timeView->xAxis->setTickStep(2);
  timeView->xAxis->setLabel("time since startup");

  // Left axis shows CoP distance from center in centimeters
  timeView->yAxis->setRange(-30,30);
  timeView->yAxis->setLabel("cm from center");

  // This matches up the top to the bottom
  timeView->axisRect()->setupFullAxesBox();
  connect(timeView->xAxis, SIGNAL(rangeChanged(QCPRange)), timeView->xAxis2, SLOT(setRange(QCPRange)));

  // The right axis shows mass in kg (assuming normal gravity)
  timeView->yAxis2->setTickLabelType(QCPAxis::ltNumber);
  timeView->yAxis2->setRange(-5,120);  // Value in kg
  timeView->yAxis2->setLabel("kg");
  timeView->yAxis2->setTickLabels(true);

}



//virtual
/**
 * @brief MainWindow::nativeEvent passes DEVICECHANGE events into our
 * bluetooth interface object.
 * @param eventType
 * @param message
 * @param result
 * @return
 */
bool MainWindow::nativeEvent(const QByteArray & /*eventType*/, void * message, long * result)
{
  MSG* msg = reinterpret_cast<MSG*>(message);

  if (WM_DEVICECHANGE==msg->message) {
    return mybt.deviceChange(msg,result);
  }
  return false;
}

/**
 * @brief MainWindow::resizeEvent
 * @param event
 *
 * This works to re-set the aspect ratio appropriately.
 *
 */
//void MainWindow::resizeEvent ( QResizeEvent * event )
//{
//  QMainWindow::resizeEvent(event);
//  if (fullView && fullView->isVisible()) {
//    fullView->yAxis->setScaleRatio(fullView->xAxis);
//  }
//}

#include "pairingdialog.h"
#include "ui_pairingdialog.h"

PairingDialog::PairingDialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::PairingDialog)
{
  ui->setupUi(this);

  connect(ui->activePairPushButton,SIGNAL(toggled(bool)),
          this,SLOT(pairingToggle(bool)));
  connect(ui->closePushButton,SIGNAL(clicked()),
          this,SLOT(close()));
}

PairingDialog::~PairingDialog()
{
  delete ui;
}

void PairingDialog::pairingToggle(bool toggle)
{
  emit pairing(toggle);
}

void PairingDialog::endPairing()
{
  ui->activePairPushButton->setChecked(false);
}

void PairingDialog::closeEvent(QCloseEvent* event)
{
  ui->activePairPushButton->setChecked(false);
  QDialog::closeEvent(event);
}

#include "wiidevice.h"

#include "wiiuse.h"

#include "globaltime.h"

WiiDevice::WiiDevice(wiimote_t* dev, QObject *parent) :
  QObject(parent),
  wm(dev),
  boardCheck(false)
{
}

/**
 * @brief WiiDevice::handleEvent
 *
 * This is either a button push/release or it's
 * a frame of streamed data.  Announce
 * the arrival of new data.
 */
void WiiDevice::handleEvent()
{
  if (IS_JUST_PRESSED(wm, WIIMOTE_BUTTON_A)) {
    emit buttonPress(ButtonA);
  }

  if (IS_RELEASED(wm, WIIMOTE_BUTTON_A)) {
    emit buttonRelease(ButtonA);
  }

  // Spit out the accelerations
  if (WIIUSE_USING_ACC(wm) && wm->accel_calib.cal_g.x>0) {
    emit accelFrame(wm->gforce.x,
                    wm->gforce.y,
                    wm->gforce.z,
                    GlobalTime::elapsed());
  }


  if (wm->exp.type == EXP_WII_BOARD) {
    /* wii balance board */
    struct wii_board_t* wb = (wii_board_t*)&wm->exp.wb;
//    float total = wb->tl + wb->tr + wb->bl + wb->br;
//    float x = 0;
//    float y = 0;

//    if (total>.5) {
//      // Convert to centimeters:
//      //  balance board is approximately 50cm x 30cm
//      x = 25*(((wb->tr + wb->br) / total) * 2 - 1);
//      y = -15*(((wb->tl + wb->tr) / total) * 2 - 1);
//    }
    emit boardFrame(wb->tl,wb->tr,wb->bl,wb->br,GlobalTime::elapsed());
  }
}


void WiiDevice::requestStatus()
{
  wiiuse_status(wm);
}

/**
 * @brief WiiDevice::handleStatus
 *
 * This is mostly useful for figuring out the
 * state of the battery.  I think it
 * also tells us if there are any
 * peripherals attached and what LEDs are lit.
 */
void WiiDevice::handleStatus()
{
  if (!boardCheck && wm->exp.type!=EXP_WII_BOARD) {
    wiiuse_motion_sensing(wm,1);
    boardCheck=true;
  }
  emit batteryLevel(wm->battery_level);
}

/**
 * @brief WiiDevice::poll check our device for new packets.
 */
void WiiDevice::poll()
{
  wiiuse_poll(&wm, 1);
}

void WiiDevice::processEvents()
{
  switch (wm->event) {
    case WIIUSE_EVENT:
      // This is a button-state change
      // or a streaming data packet.
      handleEvent();
      break;
    case WIIUSE_STATUS:
      // a status event occurred
      // announce the battery levels
      handleStatus();
      break;

    case WIIUSE_DISCONNECT:
    case WIIUSE_UNEXPECTED_DISCONNECT:
      // the wiimote disconnected
      // handle_disconnect();
      break;

    case WIIUSE_READ_DATA:
    case WIIUSE_NUNCHUK_INSERTED:
    case WIIUSE_CLASSIC_CTRL_INSERTED:
    case WIIUSE_WII_BOARD_CTRL_INSERTED:
    case WIIUSE_GUITAR_HERO_3_CTRL_INSERTED:
    case WIIUSE_MOTION_PLUS_ACTIVATED:
    case WIIUSE_NUNCHUK_REMOVED:
    case WIIUSE_CLASSIC_CTRL_REMOVED:
    case WIIUSE_GUITAR_HERO_3_CTRL_REMOVED:
    case WIIUSE_WII_BOARD_CTRL_REMOVED:
    case WIIUSE_MOTION_PLUS_REMOVED:
    default:
      // We don't need to handle these events
      // right now
      break;
  }
}


void WiiDevice::zeroCalibration()
{
  wiiuse_set_wii_board_calib(wm);
}





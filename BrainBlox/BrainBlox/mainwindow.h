#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTabWidget>
#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsItem>
#include <QTimer>
#include <QList>

#include "wiiusehandler.h"
#include "plotsceneview.h"
#include "bufferscrollcontrol.h"
#include "bluetoothinterface.h"
#include "pairingdialog.h"


class QToolButton;
class QCustomPlot;
class FullCustomPlot;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = 0);
  ~MainWindow();

public slots:

  void updateView();

  void tabChanged(int);

  void connectPrepare();
  void launchPairing();
  void loadBoardDialog();
  void loadAccelDialog();

  void recordAll();
  void stopAll();
  void clearData();
  void saveData();

  void createBuffer(WiiDevice* device);
  void cloneBuffer(const std::vector<BoardFrame>& staticData);
  void cloneBuffer(const std::vector<AccelFrame>& staticData);
  void closeBuffer(BufferScrollControl* buffer);

  void about();
  void aboutQt();
  void tips();

protected:

  void initializeCustomPlot();

protected:
  /// This function captures native Windows events.
  /// We use it to process bluetooth events.
  virtual bool nativeEvent(const QByteArray &, void * message, long * result);

//  virtual void resizeEvent ( QResizeEvent * event );

private:
  Ui::MainWindow *ui;

  PlotSceneView* recentView;
  QCustomPlot* timeView;
  FullCustomPlot* fullView;

  // indices of our views within the
  // tab widget
  int recentIdx;
  int timeIdx;
  int fullIdx;

  int dataBuffs;
  int devBuffs;

  QList<QAbstractGraphicsShapeItem*> itemTrail;


  QPointF lastPoint;
  qreal lastR;
  qreal velX,velY,velR;
  qreal velH;


  QTimer recordTimer;       ///< Automatically stop recording

  QTimer deviceTimer; ///< Poll Devices for new data.
  QTimer viewTimer;   ///< Grab data from the Buffers and update the active Views
  QTimer statusTimer; ///< Periodically ask for status updates

  int trialSeconds;


  WiiuseHandler* handler;


  QList<BufferScrollControl*> viewControlList;

  BluetoothInterface mybt;

  PairingDialog* pDialog;

};

#endif // MAINWINDOW_H

#ifndef DATAFRAME_H
#define DATAFRAME_H

struct DataFrame
{
  qint64 tStamp; ///< Time stamp

  DataFrame()
      : tStamp(0)
  {}
  float ms() { return tStamp/1000.0f; }

};

struct BoardFrame : public DataFrame
{
  float tl,tr,bl,br; ///< Force on each corner

  BoardFrame()
    : tl(0),tr(0),bl(0),br(0)
  {}

  // Don't report x,y unless force exceeds a threshold
  float x() { float m = f(); return m>.5?50*((tr+br)/f()-.5f):0; }
  float y() { float m = f(); return m>.5?30*((bl+br)/f()-.5f):0; }
  float f() { return tl+tr+bl+br; }

};

struct AccelFrame : public DataFrame
{
  float ax,ay,az;

  AccelFrame()
    : ax(0),ay(0),az(0)
  {}

  float mag() { return sqrt(ax*ax+ay*ay+az*az); }
  float x() { float m = mag(); return (m>0)?-ax/m:0; }
  float y() { float m = mag(); return (m>0)?-ay/m:0; }
};

#endif // DATAFRAME_H

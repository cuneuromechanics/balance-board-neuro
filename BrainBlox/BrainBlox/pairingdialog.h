#ifndef PAIRINGDIALOG_H
#define PAIRINGDIALOG_H

#include <QDialog>

namespace Ui {
class PairingDialog;
}

class PairingDialog : public QDialog
{
  Q_OBJECT

public:
  explicit PairingDialog(QWidget *parent = 0);
  ~PairingDialog();

signals:
  void pairing(bool active);

public slots:
  void pairingToggle(bool toggle);
  void endPairing();

protected:
  virtual void closeEvent(QCloseEvent* event);

private:
  Ui::PairingDialog *ui;
};

#endif // PAIRINGDIALOG_H

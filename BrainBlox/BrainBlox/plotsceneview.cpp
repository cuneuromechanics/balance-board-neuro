#include "plotsceneview.h"

#include <QMessageBox>
#include <QResizeEvent>

PlotSceneView::PlotSceneView(QWidget *parent) :
  QGraphicsView(parent)
{
  setScene(new QGraphicsScene);

  QRectF r(0,0,25*50,25*30);
  r.translate(-r.width()/2,-r.height()/2);
}


void PlotSceneView::resizeEvent ( QResizeEvent * event )
{
  QGraphicsView::resizeEvent(event);
  QRect viewRect = viewport()->rect();

  // Figure out the pixel to cm mapping
  emit viewSize(viewRect.width(),viewRect.height());
}

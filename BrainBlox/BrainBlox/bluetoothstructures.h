#ifndef BLUETOOTHSTRUCTURES_H
#define BLUETOOTHSTRUCTURES_H

#pragma warning(push)
#pragma warning(disable:4068)
#include <Windows.h>
#include <BluetoothAPIs.h>
#include <Dbt.h>
#include <Ws2bth.h>
#pragma warning(pop)

/**
 * @brief The BluetoothStructures class holds Bluetooth-specific
 * structures so that we can access them without including the
 * Bluetooth API headers everywhere.
 */
class BluetoothStructures
{
public:
  BluetoothStructures();

public:
  BLUETOOTH_RADIO_INFO radioInfo;

};

#endif // BLUETOOTHSTRUCTURES_H

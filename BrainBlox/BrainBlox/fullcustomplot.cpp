#include "fullcustomplot.h"

FullCustomPlot::FullCustomPlot(QWidget *parent) :
  QCustomPlot(parent)
{
  this->xAxis->setRange(-26,26);
  this->yAxis->setRange(-16,16);
  this->yAxis->setScaleRatio(this->xAxis);
  this->xAxis->setLabel("cm");
}


/**
 * @brief MainWindow::resizeEvent
 * @param event
 *
 * This works to re-set the aspect ratio appropriately.
 *
 */
void FullCustomPlot::resizeEvent ( QResizeEvent * event )
{
  QCustomPlot::resizeEvent(event);
  this->yAxis->setScaleRatio(this->xAxis);
}

/**
 * @brief FullCustomPlot::showEvent
 * @param event
 *
 * Try to make the plot adjust when
 * changing tabs.  Still no luck. Grr.
 */
void FullCustomPlot::showEvent ( QShowEvent * event )
{
  QCustomPlot::showEvent(event);
  this->yAxis->setScaleRatio(this->xAxis);
}

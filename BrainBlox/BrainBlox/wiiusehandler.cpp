#include "wiiusehandler.h"

#include "wiiuse.h"

#include <stdlib.h>
// Prevent c++ mangling
extern "C" {
  #include <hidsdi.h>
  #include <setupapi.h>
}

// Device IDs from Wiiuse
#define WM_VENDOR_ID				0x057E
#define WM_PRODUCT_ID				0x0306

WiiuseHandler::WiiuseHandler(QObject *parent) :
  QObject(parent)
{
  // This mostly just allocates memory.
  //wiimotes = wiiuse_init(MAX_WIIMOTES);
}

WiiuseHandler::~WiiuseHandler()
{
  //wiiuse_cleanup(wiimotes,MAX_WIIMOTES);
}


/**
 * @brief WiiuseHandler::wrapNewDevice
 * @param dev
 *
 * We create a wrapper around the wiiuse struct
 * so that we can report events.
 *
 * We announce the arrival of the object.
 *
 * We add it to our list of objects to be polled.
 */
void WiiuseHandler::wrapNewDevice(const QString& fid, wiimote_t* dev)
{
  WiiDevice* newDev = new WiiDevice(dev,this);
  wiiuse_set_leds(dev,WIIMOTE_LED_1);
  deviceMap[fid] = newDev;
  emit newDevice(newDev);
}

/**
 * @brief WiiuseHandler::poll
 * The wiiuse library uses a polling model that
 * requires us to ask periodically if anything
 * has changed.
 * This seems silly since the asynchronous IO
 * provides an event-driven model.
 *
 * If we're connected to any devices, check
 * to see if that device has any outstanding
 * events and process them.
 */
void WiiuseHandler::poll()
{
  auto mapEnd = deviceMap.end();

  for (auto mapIt = deviceMap.begin();mapIt!=mapEnd;++mapIt) {
    mapIt.value()->poll();
  }
  for (auto mapIt = deviceMap.begin();mapIt!=mapEnd;++mapIt) {
    mapIt.value()->processEvents();
  }
}

/*
 *
 * Find and connect to wiiuse devices.
 *
 * Under Linux, wiiuse_find() actually goes
 * out and looks for discoverable Bluetooth devices.
 * wiiuse_connect() opens the right Bluetooth
 * L2CAP ports for communication.
 *
 * Under Windows, wiiuse_find() looks for HID
 * objects that are already connected.  The pairing
 * and connection have to be formed by the OS.
 * The find function grabs handles to the devices.
 * wiiuse_connect() doesn't do anything under Windows.
 *
 */
//void WiiuseHandler::initDevices(bool toOpen)
//{
//  if (toOpen) {
//    wiiuse_find(wiimotes, MAX_WIIMOTES, 5);
//    devCount = wiiuse_connect(wiimotes, MAX_WIIMOTES);
//    for (int ii=0;ii<devCount;++ii) {
//      device[ii] = new WiiDevice(wiimotes[ii],this);
//      wiiuse_set_leds(wiimotes[ii],WIIMOTE_LED_1);
//      emit newDevice(device[ii]);
//    }
//    emit connected(devCount);
//  } else {
//    //?
//  }
//}

/**
 * @brief WiiuseHandler::findNewDevices uses windows api to
 * check for new wii devices.  This way, the device can
 * connect during execution and we'll find it.
 *
 * This replaces functionality in the wiiuse_find() function.
 *
 */
void WiiuseHandler::findNewDevices()
{
  GUID device_id;
  HANDLE dev;
  HDEVINFO device_info;
  int i, index;
  DWORD len;
  SP_DEVICE_INTERFACE_DATA device_data;
  PSP_DEVICE_INTERFACE_DETAIL_DATA detail_data = nullptr;
  HIDD_ATTRIBUTES	attr;
  wiimote_t* wm = nullptr;

  device_data.cbSize = sizeof(device_data);
  index = 0;

  // We're going to be asking for HID type objects
  HidD_GetHidGuid(&device_id);

  // Ask for all currently connected HIDs
  device_info = SetupDiGetClassDevs(&device_id, NULL, NULL, (DIGCF_DEVICEINTERFACE | DIGCF_PRESENT));

  // Check all connected HIDs to see if they're wii devices
  for (;; ++index) {
    if (detail_data) {
      free(detail_data);
      detail_data = nullptr;
    }

    // Get a handle to the next attached HID
    if (!SetupDiEnumDeviceInterfaces(device_info, NULL, &device_id, index, &device_data)) {
      // There's nothing left to find.  We're out of devices.
      break;
    }

    // Figure out how much memory is needed to hold
    // information about this device.
    SetupDiGetDeviceInterfaceDetail(device_info, &device_data, NULL, 0, &len, NULL);

    // Allocate memory to hold device info.
    detail_data = (PSP_DEVICE_INTERFACE_DETAIL_DATA)malloc(len);
    detail_data->cbSize = sizeof(SP_DEVICE_INTERFACE_DETAIL_DATA);

    // query the data for this device
    if (!SetupDiGetDeviceInterfaceDetail(device_info, &device_data, detail_data, len, NULL, NULL)) {
      continue;
    }

    // Check the DevicePath against our device map.
    QString devHandle = QString::fromWCharArray(detail_data->DevicePath);
    if (deviceMap.contains(devHandle)) {
      // We already have a handle to this device
      continue;
    }

    // This is a new device
    // Try opening a handle to it
    dev = CreateFile(detail_data->DevicePath,
            (GENERIC_READ | GENERIC_WRITE),
            (FILE_SHARE_READ | FILE_SHARE_WRITE),
            NULL, OPEN_EXISTING, FILE_FLAG_OVERLAPPED, NULL);
    if (dev == INVALID_HANDLE_VALUE)
      continue;

    // get device attributes
    attr.Size = sizeof(attr);
    i = HidD_GetAttributes(dev, &attr);

    // Check to see if the device is a wii device.
    // Rumor has it that recent devices use a different
    // Product ID.  If so, we'll need to check against
    // that too.
    if ((attr.VendorID == WM_VENDOR_ID) && (attr.ProductID == WM_PRODUCT_ID)) {
      // this is a wiimote or board, but it might not actually
      // be connected right now.
      // We check by trying to write a packet.

      // Here's the MAC address
      // WCHAR str[128];
      // if (HidD_GetSerialNumberString(dev,str,128)) {
      // }

      // Create the device in the wiiuse library
      wm = wiiuse_open_handle(dev);
      if (wm) {
        // Hooray!
        wrapNewDevice(devHandle,wm);
      } else {
        // This happens when a wii device was recently
        // connected, but is not currently connected.
        CloseHandle(dev);
      }
    } else {
      // Device is not a wiimote as far as we can tell
      CloseHandle(dev);
    }
  }

  // Cleanup memory we've allocated.
  if (detail_data) {
    free(detail_data);
    detail_data = nullptr;
  }
  SetupDiDestroyDeviceInfoList(device_info);
}

void WiiuseHandler::disconnect()
{

}

void WiiuseHandler::requestStatus()
{
  auto mapEnd = deviceMap.end();

  for (auto mapIt = deviceMap.begin();mapIt!=mapEnd;++mapIt) {
    mapIt.value()->requestStatus();
  }
}


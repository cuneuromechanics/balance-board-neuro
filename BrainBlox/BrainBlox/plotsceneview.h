#ifndef PLOTSCENEVIEW_H
#define PLOTSCENEVIEW_H

#include <QGraphicsView>

#include <QGraphicsScene>
#include <QGraphicsEllipseItem>
#include <vector>

class PlotSceneView : public QGraphicsView
{
  Q_OBJECT
public:
  explicit PlotSceneView(QWidget *parent = 0);

signals:

  void viewSize(int w,int h);

protected:

  void resizeEvent ( QResizeEvent * event );


};

#endif // PLOTSCENEVIEW_H
